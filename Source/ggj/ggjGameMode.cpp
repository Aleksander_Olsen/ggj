// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.

#include "ggj.h"
#include "ggjGameMode.h"
#include "ggjPlayerController.h"
#include "ggjCharacter.h"

AggjGameMode::AggjGameMode(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	// use our custom PlayerController class
	PlayerControllerClass = AggjPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/MyCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}