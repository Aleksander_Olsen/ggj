// Copyright 1998-2015 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "ggjGameMode.generated.h"

UCLASS(minimalapi)
class AggjGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	AggjGameMode(const FObjectInitializer& ObjectInitializer);
};



